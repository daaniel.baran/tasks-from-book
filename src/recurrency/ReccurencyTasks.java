package recurrency;

import java.util.List;

public class ReccurencyTasks {

    public static int sum(int[] array) {
        int sum = 0;
        if (array.length == 0) {
            return sum;
        } else {
            int[] newArr = new int[array.length - 1];
            for (int i = 0; i < array.length - 1; i++) newArr[i] = array[i];
            sum += array[array.length - 1];
            return sum + sum(newArr);
        }
    }

    public static int countElements(List<Integer> list) {

        return 0;
    }

    public static int getHighestInList(List<Integer> list) {

        return 0;
    }

    public static void main(String[] args) {
        int[] test = {2, 3, 4, 5};

        System.out.println(sum(test));
    }
}
