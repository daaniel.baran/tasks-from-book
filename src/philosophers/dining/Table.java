package philosophers.dining;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Table {
    private List<Chopstick> chopsticks = new ArrayList<>();
    private List<Philosopher> philosophers = new ArrayList<>();

    public void feedPhilosophers(int number) {
        for (int j = 0; j <= number; j++)
            chopsticks.add(new Chopstick(j));

        for (int i = 0; i <= number; i++)
            philosophers.add(new Philosopher(i, chopsticks));

        ExecutorService executorService = Executors.newFixedThreadPool(number);
        philosophers.forEach(executorService::execute);
    }


    public static void main(String[] args) {
        Table table = new Table();
        table.feedPhilosophers(6);
    }
}
