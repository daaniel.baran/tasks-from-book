package philosophers.dining;

public class Chopstick {

    private boolean free;
    private int id;

    public Chopstick(int id) {
        this.free = true;
        this.id = id;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Chopstick{" +
                "free=" + free +
                ", id=" + id +
                '}';
    }
}
