package philosophers.dining;

import java.util.List;

public class Philosopher implements Runnable {

    private int id;
    private Enum state;
    private List<Chopstick> chopsticks;
    private boolean isFull;
    private Chopstick left;
    private Chopstick right;

    public Philosopher(int id, List<Chopstick> chopsticks) {
        this.id = id;
        this.chopsticks = chopsticks;
        this.left = chopsticks.get(id);
        this.right = chopsticks.get((id + 1) % chopsticks.size());
        this.state = State.WAITS;
        this.isFull = false;
    }

    public int getId() {
        return id;
    }

    public Enum getState() {
        return state;
    }

    public void setState(Enum state) {
        this.state = state;
    }

    private enum State {
        EATS, WAITS;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        long end = start + 5000;

        while (!isFull && isAbleToEat()) {
            getChopsticks();
            if (System.currentTimeMillis() < end) {
                releaseChopsticks();
                break;
            }
        }
        System.out.println("PHILOSOPHER-" + getId() + " " + getState());
    }

    synchronized boolean isAbleToEat() {
        return left.isFree() && right.isFree();
    }

    synchronized void getChopsticks() {
        if (isAbleToEat()) {
            left.setFree(false);
            right.setFree(false);
            setState(State.EATS);
            System.out.println("PHILOSOPHER-" + getId() + " " + getState() + " [" + left.getId() + "," + right.getId() + "]");
        }
    }

    synchronized void releaseChopsticks() {
        isFull = true;
        if (!left.isFree() && !right.isFree()) {
            left.setFree(true);
            right.setFree(true);
            setState(State.WAITS);
            System.out.println("CHOPSTICKS [" + left.getId() + "," + right.getId() + "] RELEASED!");
        }
    }
}
